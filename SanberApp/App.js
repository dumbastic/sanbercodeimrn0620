import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import YoutubeUI from './Tugas/Tugas12/App';
import Register from './Tugas/Tugas13/RegisterScreen';
import Login from './Tugas/Tugas13/LoginScreen';
import About from './Tugas/Tugas13/AboutScreen';
import ToDoApps from './Tugas/Tugas14/App'
import SkillProgramming from './Tugas/Tugas14/SkillScreen'
import Navigation1 from './Tugas/Tugas15/index'
import Navigation2 from './Tugas/TugasNavigation/index'
import Quiz3 from './Quiz3/index'

export default function App() {
  return (
    // <YoutubeUI />
    // <Register />
    // <Login />
    // <About />
    // <ToDoApps />
    // <SkillProgramming />
    // <Navigation1 />
    // <Navigation2 />
    <Quiz3 />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
