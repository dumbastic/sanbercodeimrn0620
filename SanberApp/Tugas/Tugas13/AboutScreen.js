import React, { Component } from 'react'
import { View, Image, Text, StyleSheet } from 'react-native'

import { FontAwesome } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';

export default class App extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={[styles.title, styles.darkBlue]}>Tentang Saya</Text>
                <FontAwesome name="user-circle-o" size={200} style={styles.userPic} />
                <Text style={[styles.name, styles.darkBlue]}>Mukhlis Hanafi</Text>
                <Text style={[styles.job, styles.lightBlue]}>React Native Developer</Text>
                <View style={styles.panel}>
                    <Text style={[styles.subTitle, styles.darkBlue]}>Portofolio</Text>
                    <View style={{flexDirection: "row", justifyContent: "space-evenly", padding: 10}}>
                        <View style={styles.logoVertical}>
                            <AntDesign name="gitlab" size={40} 
                                style={[styles.lightBlue, {marginBottom: 5}]} />
                            <Text style={[styles.socmedAccount, styles.darkBlue]}>@mukhlish</Text>
                        </View>
                        <View style={styles.logoVertical}>
                            <AntDesign name="github" size={40} 
                                style={[styles.lightBlue, {marginBottom: 5}]} />
                            <Text style={[styles.socmedAccount, styles.darkBlue]}>@mukhlis-h</Text>
                        </View>
                    </View>                    
                </View>
                <View style={styles.panel}>
                    <Text style={[styles.subTitle, styles.darkBlue]}>Hubungi Saya</Text>
                    <View style={{alignItems: "center"}}>
                        <View style={styles.logoHorizontal}>
                            <AntDesign name="facebook-square" size={40} 
                                style={[styles.lightBlue, {marginRight: 10}]} />
                            <Text style={[styles.socmedAccount, styles.darkBlue]}>mukhlis.hanafi</Text>
                        </View>
                        <View style={styles.logoHorizontal}>
                            <AntDesign name="instagram" size={40} 
                                style={[styles.lightBlue, {marginRight: 10}]} />
                            <Text style={[styles.socmedAccount, styles.darkBlue]}>@mukhlis_hanafi</Text>
                        </View>
                        <View style={styles.logoHorizontal}>
                            <AntDesign name="twitter" size={40} 
                                style={[styles.lightBlue, {marginRight: 10}]} />
                            <Text style={[styles.socmedAccount, styles.darkBlue]}>@mukhlish</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    title: {
        fontSize: 36,
        fontWeight: "bold",
        margin: 10
    },
    userPic: {
        margin: 10,
        color: "#EFEFEF"
    },
    name: {
        fontSize: 24,
        fontWeight: "bold"
    },
    job: {
        fontSize: 16,
        fontWeight: "bold"
    },
    socmedAccount: {
        fontSize: 16,
        fontWeight: "bold"
    },
    panel: {
        width: "90%",
        marginTop: 10,
        padding: 10,
        borderRadius: 16,
        backgroundColor: "#EFEFEF"
    },
    subTitle: {
        fontSize: 18,
        borderBottomWidth: 1,
        borderBottomColor: "#003366"
    },
    logoVertical: {
        alignItems: "center"
    },
    logoHorizontal: {
        flexDirection: 'row',
        width: "50%",
        padding: 10,
        alignItems: "center"
    },
    darkBlue: {
        color: '#003366'
    },
    lightBlue: {
        color: '#3EC6FF'
    }
  });