import React, { Component } from 'react';
import { View, Image, Text, ScrollView, StyleSheet, FlatList } from 'react-native';

import { MaterialCommunityIcons } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';

import data from './skillData.json';

class SkillItem extends Component {
    render() {
        let skill = this.props.skill;
        return (
            <View style={[styles.containerItem, styles.blueBackground]}>
                <MaterialCommunityIcons name={skill.iconName} size={75} color="#003366" style={{width: "25%"}} />
                <View style={{flexDirection: "column", width: "50%"}}>
                    <Text style={[{fontSize: 24, fontWeight: "bold"}, styles.darkBlue]}>
                        {skill.skillName}
                    </Text>
                    <Text style={[{fontSize: 16, fontWeight: "bold"}, styles.lightBlue]}>
                        {skill.categoryName}
                    </Text>
                    <Text style={[{fontSize: 48, fontWeight: "bold", color: "white", alignSelf: "flex-end"}]}>
                        {skill.percentageProgress}
                    </Text>
                </View>
                <MaterialIcons name="keyboard-arrow-right" size={100} color="#003366" style={{width: "22%"}} />
            </View>
        )
    }
}

export default class Main extends Component {
    render() {
        return(
            <View style={styles.container}>
                <Image source={require('../Tugas13/images/logo.png')} style={styles.logo} />
                <View style={styles.body}>
                    <View style={{flexDirection: "row", justifyContent: "flex-start", alignItems: "center"}}>
                        <FontAwesome name="user-circle" size={26} color="#3EC6FF" style={{marginRight:10}} />
                        <View>
                            <Text style={[{fontSize:12}, styles.darkBlue]}>Hai,</Text>
                            <Text style={[{fontSize:16}, styles.darkBlue]}>Mukhlis Hanafi</Text>
                        </View>
                    </View>
                    <Text style={[styles.title, styles.darkBlue]}>SKILL</Text>
                    <View style={{flexDirection: "row", justifyContent: "space-between", marginBottom: 5}}>
                        <Text style={[styles.category, styles.darkBlue, styles.blueBackground]}>
                            Framework / Library
                        </Text>
                        <Text style={[styles.category, styles.darkBlue, styles.blueBackground]}>
                            Bahasa Pemrograman
                        </Text>
                        <Text style={[styles.category, styles.darkBlue, styles.blueBackground]}>
                            Teknologi
                        </Text>
                    </View>
                    <FlatList
                        data={data.items}
                        renderItem={(skill)=><SkillItem skill={skill.item} />}
                        keyExtractor={(item)=>item.id.toString()}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    containerItem: {
        flexDirection: "row", 
        justifyContent: "flex-end",
        alignItems: "center",
        marginBottom: 7,
        paddingTop: 10,
        borderRadius: 8,
        shadowColor: "black",
        shadowOffset:{width: 4,  height: 4},
        shadowRadius: 0,
        shadowOpacity: 0.25,
        elevation: 5
    },
    logo: {
        height: 51,
        width: 187.5,
        alignSelf: "flex-end"
    },
    body: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20
    },
    title: {
        fontSize: 36,
        borderBottomWidth: 4,
        borderBottomColor: "#3EC6FF",
        marginTop: 10,
        marginBottom: 10
    },
    category: {
        fontSize: 12,
        fontWeight: "bold",
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 8
    },
    darkBlue: {
        color: "#003366"
    },
    lightBlue: {
        color: "#3EC6FF"
    },
    blueBackground: {
        backgroundColor: "#B4E9FF"
    }
})