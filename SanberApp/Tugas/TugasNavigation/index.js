import React from "react"

import { NavigationContainer, DefaultTheme } from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { createDrawerNavigator } from "@react-navigation/drawer"

import LoginScreen from "../Tugas13/LoginScreen"
import AboutScreen from "../Tugas13/AboutScreen"
import SkillScreen from "../Tugas14/SkillScreen"
import ProjectScreen from "./ProjectScreen"
import AddScreen from "./AddScreen"

import Icon from 'react-native-vector-icons/Octicons';

const Skill = createStackNavigator()
const SkillScreens = () => (
    <Skill.Navigator>
        <Skill.Screen name="Skill" component={SkillScreen} />
    </Skill.Navigator>
)

const Project = createStackNavigator()
const ProjectScreens = () => (
    <Project.Navigator>
        <Project.Screen name="Project" component={ProjectScreen} />
    </Project.Navigator>
)

const Add = createStackNavigator()
const AddScreens = () => (
    <Add.Navigator>
        <Add.Screen name="Add" component={AddScreen} />
    </Add.Navigator>
)

const Tab = createBottomTabNavigator()
const TabScreen = () => (
    <Tab.Navigator>
        <Tab.Screen name="Skill" component={SkillScreens} />
        <Tab.Screen name="Project" component={ProjectScreens} />
        <Tab.Screen name="Add" component={AddScreens} />
    </Tab.Navigator>
)

const Drawer = createDrawerNavigator()
const DrawerScreen = ({ navigation }) => (
    <Drawer.Navigator>
        <Drawer.Screen name="Home" component={TabScreen} />
        <Drawer.Screen name="About" component={AboutScreen} />
    </Drawer.Navigator>
)

const RootStack = createStackNavigator()
const RootStackScreen = () => (
    <RootStack.Navigator>
        <RootStack.Screen name="Login" component={LoginScreen} />
        <RootStack.Screen name="SanberApp" component={DrawerScreen} 
            options={({ navigation, route }) => ({ 
                headerLeft: () => (
                    <Icon name='three-bars' size={30} color="black" style={{marginLeft: 20}} 
                        onPress={() => 
                            // navigation.toggleDrawer()
                            alert('TODO: implement navigation.toggleDrawer()')
                        } />
                )
            })} 
        />
    </RootStack.Navigator>
)

export default () => {
    return (
        <NavigationContainer theme={MyTheme}>
            <RootStackScreen />
        </NavigationContainer>
    )
}

const MyTheme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      background: 'white'
    },
};