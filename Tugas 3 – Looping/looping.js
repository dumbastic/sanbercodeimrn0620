// Soal No.1 Looping while
var i = 0;
console.log("Soal No.1");
console.log("LOOPING PERTAMA");
while (i < 20) {
    i+=2;
    console.log(i + " - I love coding");
}

console.log("LOOPING KEDUA");
while (i > 0) {
    console.log(i + " - I will become a mobile developer");
    i-=2;
}
console.log();

// Soal No.2 Looping menggunakan for
console.log("Soal No.2");
for (var i = 1; i <= 20; i++) {
    if (i % 2 == 1) {
        if (i % 3 == 0) {
            console.log(i + " - I Love Coding");
        }
        else {
            console.log(i + " - Santai");
        }        
    }
    else if (i % 2 == 0) {
        console.log(i + " - Berkualitas");
    }
}
console.log();

// Soal No.3 Membuat Persegi Panjang
console.log("Soal No.3");
for (var i = 0; i < 4; i++) {
    for (var j = 0; j < 8; j++) {
        process.stdout.write("#");
    }
    console.log();
}
console.log();

// Soal No.4 Membuat Tangga
console.log("Soal No.4");
for (var i = 0; i < 7; i++) {
    for (var j = 0; j < i+1; j++) {
        process.stdout.write("#");
    }
    console.log();
}
console.log();

// Soal No.5 Membuat Papan Catur
console.log("Soal No.5");
for (var i = 0; i < 8; i++) {
    for (var j = 0; j < 8; j++) {
        if(i % 2 == 0 && j % 2 == 1 || i % 2 == 1 && j % 2 == 0) {
            process.stdout.write("#");
        }
        else {
            process.stdout.write(" ");
        }
    }
    console.log();
}