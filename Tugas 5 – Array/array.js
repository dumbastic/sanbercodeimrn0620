// Soal No.1 Range
function range(startNum, finishNum) {
    if (startNum == null || finishNum == null) {
        return -1;
    }
    else {
        var array = [];
        if (startNum < finishNum) {
            for (var i = startNum; i < finishNum+1; i++) {
                array.push(i);
            }
        }
        else {
            for (var i = finishNum; i < startNum+1; i++) {
                array.unshift(i);
            }
        }
        return array;
    }
}

console.log("Soal No.1");
console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());
console.log();

// Soal No.2 Range with Step
function rangeWithStep(startNum, finishNum, step) {
    if (startNum == null) {
        return -1;
    }
    else if (finishNum == null) {
        return [startNum];
    }
    else {
        if (step == null) {
            step = 1;
        }
        var array = [];
        if (startNum < finishNum) {
            for (var i = startNum; i < finishNum+1; i+=step) {
                array.push(i);
            }
        }
        else {
            for (var i = startNum; i > finishNum-1; i-=step) {
                array.push(i);
            }
        }
        return array;
    }    
}

console.log("Soal No.2");
console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));
console.log();

// Soal No.3 Sum of Range
function sum(startNum, finishNum, step) {
    var array = rangeWithStep(startNum, finishNum, step);
    var sum = 0;
    for (var i in array) {
        sum += array[i];
    }
    return sum;
}

console.log("Soal No.3");
console.log(sum(1, 10));
console.log(sum(5, 50, 2));
console.log(sum(15, 10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());
console.log();

// Soal No.4 Array Multidimensi
function dataHandling(input) {
    var output = "";
    for (var i in input) {
        output += "Nomor ID:  " + input[i][0] + "\n";
        output += "Nama Lengkap:  " + input[i][1] + "\n";
        output += "TTL:  " + input[i][2] + " " + input[i][3] + "\n";
        output += "Hobi:  " + input[i][4] + "\n\n";
    }
    return output;
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];
console.log("Soal No.4");
console.log(dataHandling(input));

// Soal No.5 Balik Kata
function balikKata(kata) {
    var balik = "";
    for (var i = kata.length-1; i >= 0; i--) {
        balik += kata[i];
    }
    return balik;
}

console.log("Soal No.5");
console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));
console.log();

// Soal No.6 Metode Array
console.log("Soal No.6");
function dataHandling2(input) {
    input.splice(1, 1, input[1] += "Elsharawy");
    input.splice(2, 1, "Provinsi " + input[2]);
    input.splice(4, 1, "Pria", "SMA Internasional Metro");
    console.log(input);
    
    var tanggal = input[3].split("/");
    switch (tanggal[1]) {
        case "01":   { console.log("Januari"); break; }
        case "02":   { console.log("Februari"); break; }
        case "03":   { console.log("Maret"); break; }
        case "04":   { console.log("April"); break; }
        case "05":   { console.log("Mei"); break; }
        case "06":   { console.log("Juni"); break; }
        case "07":   { console.log("Juli"); break; }
        case "08":   { console.log("Agustus"); break; }
        case "09":   { console.log("September"); break; }
        case "10":   { console.log("Oktober"); break; }
        case "11":   { console.log("November"); break; }
        case "12":   { console.log("Desember"); break; }
        default:  { console.log("NA"); }
    }

    console.log(tanggal.slice().sort(function (a, b) { return b-a }));
    console.log(tanggal.join("-"));
    console.log(input[1].slice(0, 15));

    return;
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);