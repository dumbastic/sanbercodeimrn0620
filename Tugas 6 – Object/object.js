// Soal No.1 Array to Object
function arrayToObject(arr) {
    var now = new Date()
    var thisYear = now.getFullYear()
    var age
    
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].length == 4 && arr[i][3] <= thisYear) {
            age = thisYear - arr[i][3]
        }
        else {
            age = "Invalid birth year"
        }
        
        var obj = {
            firstname: arr[i][0],
            lastname: arr[i][1],
            gender: arr[i][2],
            age: age
        }
        
        process.stdout.write((i+1) + ". " + arr[i][0] + " " + arr[i][1] + ": ")
        console.log(obj)
    }
}
 
console.log("Soal No.1")
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)
arrayToObject([])
console.log()

// Soal no.2 Shopping Time
function shoppingTime(memberId, money) {
    if (memberId == "" || memberId == null) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
    else if (money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    }
    else {
        var items = ["Sepatu Staccatu", "Baju Zoro", "Baju H&N", "Sweater Uniklooh", "Casing Handphone"]
        var price = [1500000, 500000, 250000, 175000, 50000]

        var i = 0;
        var changeMoney = money
        var listPurchased = []
        
        while (changeMoney > 0 && i < 5) {
            if (changeMoney >= price[i]) {
                changeMoney -= price[i]
                listPurchased.push(items[i])
            }
            i++
        }

        var obj = {
            memberId: memberId,
            money: money,
            listPurchased: listPurchased,
            changeMoney: changeMoney
        }

        return obj
    }
}

console.log("Soal No.2")
console.log(shoppingTime('1820RzKrnWn08', 2475000))
console.log(shoppingTime('82Ku8Ma742', 170000))
console.log(shoppingTime('', 2475000))
console.log(shoppingTime('234JdhweRxa53', 15000))
console.log(shoppingTime())
console.log()

// Soal No.3 Naik Angkot
function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F']
    var arrayObj = []

    for (var i = 0; i < arrPenumpang.length; i++) {
        var bayar = 0
        var naik = false
        
        for (var j = 0; j < rute.length; j++) {
            if (naik) {
                bayar += 2000
            }
            if (rute[j] == arrPenumpang[i][1]) {
                naik = true
            }
            else if(rute[j] == arrPenumpang[i][2]) {
                naik = false
            }
        }

        var obj = {
            penumpang: arrPenumpang[i][0],
            naikDari: arrPenumpang[i][1],
            tujuan: arrPenumpang[i][2],
            bayar: bayar
        }

        arrayObj.push(obj)
    }
    return arrayObj
}

console.log("Soal No.3")
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]))
console.log(naikAngkot([]))